import * as BaseJoi from "@hapi/joi";
import * as fs from "fs";
const JoiDate = require("joi-date-extensions");
// const Joi : ExtendedJoi = BaseJoi.extend(JoiDate);
import * as Joi from "@hapi/joi";
import * as _ from "lodash";
import * as path from "path";
import {BaseService} from "./ServicesManager";
interface ExtendedJoi extends Joi {

}

export class ConfigurationService implements BaseService {
    private config: any;

    constructor(private configFile: string = "config.json") {
    }

    public setup(): void {
        this.loadConfig();
    }

    /**
     * Searches for a valid object at the provided path.
     * i.e. foo.bar would search for an object bar as a member of foo.
     * @param configPath The objects path
     * @type T TODO implement type check support (using JOI??)
     */
    public hasConfiguration<T>(configPath: string): boolean {
        if (configPath == null) {
            throw new Error("Config path is invalid.")
        }
        if (this.config == null) {
            return false;
        }

        let accessor = _.property(configPath);
        return accessor(this.config) != null;
    }

    /**
     * Load the configuration.
     */
    private loadConfig(): void {
        if(!fs.existsSync(this.configFile)){
            throw new Error(`Config file not found: ${path.resolve(this.configFile)}`);
        }

        this.config = JSON.parse(fs.readFileSync(this.configFile).toString());
    }

    /**
     * Returns the configuration found at configPath, or the whole configuration if configPath is null/undefined.
     * @param configPath The path of the config to return.
     * @type T TODO implement better type checking.
     */
    public getConfiguration<T>(configPath?: string | null | undefined): T {
        if (configPath == null || configPath == "" || configPath == "." || configPath == "/") {
            return this.config != null ? this.config : {};
        }

        let accessor = _.property(configPath);
        return accessor(this.config) as T;
    }
}
