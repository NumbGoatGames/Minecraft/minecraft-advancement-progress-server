import {BaseService, ServicesManager} from "./ServicesManager";
import {ConfigurationService} from "./Configuration.Service";
import {MinecraftConfiguration} from "../Models/MinecraftConfiguration";
import {jsonData} from "@numb/mc-questing-progression";

export class ProgressionService implements BaseService {
    private configService: ConfigurationService;
    private minecraftConfig: MinecraftConfiguration;

    setup() {
        this.configService = ServicesManager.getService<ConfigurationService>(ConfigurationService.name);
        this.minecraftConfig = this.configService.getConfiguration("minecraft");
    }

    /**
     * Gets the progression data for the server.
     */
    public async getProgressionData(): Promise<any> {
        let install = this.minecraftConfig.servers[0];
        let path = install.path;
        let players = "world/data/ftb_lib/players";
        let teams = "world/data/ftb_lib/teams";
        let questsFile = "questpacks/normal.nbt";

        // TODO switch on type of progression.
        let data = await jsonData({root: path, players: players, teams: teams, quests: questsFile});
        return data;
    }
}
