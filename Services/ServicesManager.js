"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ServicesManager {
    constructor() {
        this.services = new Map();
    }
    static CreateInstance() {
        if (this.Instance != null) {
            return;
        }
        this.Instance = new ServicesManager();
    }
    /**
     * Registers a service that can be loaded.
     * @param service
     * @param serviceName
     */
    static registerService(service, serviceName) {
        if (service == null) {
            throw new Error("Invalid service.");
        }
        this.CreateInstance();
        if (serviceName == null) {
            serviceName = service.constructor.name;
        }
        if (this.hasService(serviceName)) {
            throw new Error(`Service with name: ${serviceName} already registered.`);
        }
        this.Instance.services.set(serviceName, service);
        if (typeof service.setup === 'function') {
            service.setup();
        }
    }
    static hasService(serviceName) {
        if (this.Instance == null) {
            return false;
        }
        return this.Instance.services.has(serviceName);
    }
    static getServices() {
        let list = [];
        if (this.Instance == null) {
            return [];
        }
        for (let service of Array.from(this.Instance.services.keys())) {
            list.push(service);
        }
        return list;
    }
    static getService(serviceName) {
        if (serviceName == null) {
            throw new Error("Service name cannot be null.");
        }
        this.CreateInstance();
        let services = this.Instance.services;
        if (services.has(serviceName)) {
            // TODO Better type checking if we can.
            return services.get(serviceName);
        }
        else {
            throw new Error(`Service ${serviceName} not found.`);
        }
    }
}
exports.ServicesManager = ServicesManager;
//# sourceMappingURL=ServicesManager.js.map