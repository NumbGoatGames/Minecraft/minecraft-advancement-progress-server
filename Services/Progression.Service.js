"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ServicesManager_1 = require("./ServicesManager");
const Configuration_Service_1 = require("./Configuration.Service");
const mc_questing_progression_1 = require("@numb/mc-questing-progression");
class ProgressionService {
    setup() {
        this.configService = ServicesManager_1.ServicesManager.getService(Configuration_Service_1.ConfigurationService.name);
        this.minecraftConfig = this.configService.getConfiguration("minecraft");
    }
    /**
     * Gets the progression data for the server.
     */
    async getProgressionData() {
        let install = this.minecraftConfig.servers[0];
        let path = install.path;
        let players = "world/data/ftb_lib/players";
        let teams = "world/data/ftb_lib/teams";
        let questsFile = "questpacks/normal.nbt";
        // TODO switch on type of progression.
        let data = await mc_questing_progression_1.jsonData({ root: path, players: players, teams: teams, quests: questsFile });
        return data;
    }
}
exports.ProgressionService = ProgressionService;
//# sourceMappingURL=Progression.Service.js.map