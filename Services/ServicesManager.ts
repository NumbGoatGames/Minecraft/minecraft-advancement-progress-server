export class ServicesManager {
    private constructor() {
    }

    private services: Map<string, any> = new Map<string, any>();

    private static Instance: ServicesManager;

    private static CreateInstance() {
        if (this.Instance != null) {
            return;
        }
        this.Instance = new ServicesManager();
    }

    /**
     * Registers a service that can be loaded.
     * @param service
     * @param serviceName
     */
    public static registerService(service: any, serviceName?: string | undefined) {
        if (service == null) {
            throw new Error("Invalid service.")
        }
        this.CreateInstance();

        if (serviceName == null) {
            serviceName = service.constructor.name;
        }

        if (this.hasService<any>(serviceName)) {
            throw new Error(`Service with name: ${serviceName} already registered.`)
        }
        this.Instance.services.set(serviceName, service);
        if (typeof service.setup === 'function') {
            service.setup();
        }
    }

    public static hasService<T>(serviceName: string): boolean {
        if (this.Instance == null) {
            return false;
        }
        return this.Instance.services.has(serviceName);
    }

    public static getServices(): string[] {
        let list = [];
        if (this.Instance == null) {
            return [];
        }
        for (let service of Array.from(this.Instance.services.keys())) {
            list.push(service);
        }

        return list;
    }

    public static getService<T>(serviceName: string): T {
        if (serviceName == null) {
            throw new Error("Service name cannot be null.")
        }
        this.CreateInstance();

        let services = this.Instance.services;
        if (services.has(serviceName)) {
            // TODO Better type checking if we can.
            return services.get(serviceName) as T;
        } else {
            throw new Error(`Service ${serviceName} not found.`)
        }
    }
}

/**
 * Services should not do much logic in constructor if they can avoid it, due to this being used multiple times.
 */
export interface BaseService {
    /**
     * Setup is an optional method that should be used for any compute expensive setup.
     */
    setup()
}
