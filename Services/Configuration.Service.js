"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const JoiDate = require("joi-date-extensions");
const _ = require("lodash");
const path = require("path");
class ConfigurationService {
    constructor(configFile = "config.json") {
        this.configFile = configFile;
    }
    setup() {
        this.loadConfig();
    }
    /**
     * Searches for a valid object at the provided path.
     * i.e. foo.bar would search for an object bar as a member of foo.
     * @param configPath The objects path
     * @type T TODO implement type check support (using JOI??)
     */
    hasConfiguration(configPath) {
        if (configPath == null) {
            throw new Error("Config path is invalid.");
        }
        if (this.config == null) {
            return false;
        }
        let accessor = _.property(configPath);
        return accessor(this.config) != null;
    }
    /**
     * Load the configuration.
     */
    loadConfig() {
        if (!fs.existsSync(this.configFile)) {
            throw new Error(`Config file not found: ${path.resolve(this.configFile)}`);
        }
        this.config = JSON.parse(fs.readFileSync(this.configFile).toString());
    }
    /**
     * Returns the configuration found at configPath, or the whole configuration if configPath is null/undefined.
     * @param configPath The path of the config to return.
     * @type T TODO implement better type checking.
     */
    getConfiguration(configPath) {
        if (configPath == null || configPath == "" || configPath == "." || configPath == "/") {
            return this.config != null ? this.config : {};
        }
        let accessor = _.property(configPath);
        return accessor(this.config);
    }
}
exports.ConfigurationService = ConfigurationService;
//# sourceMappingURL=Configuration.Service.js.map