"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const Profile_Controller_1 = require("./Controllers/Profile.Controller");
const ServicesManager_1 = require("./Services/ServicesManager");
const Configuration_Service_1 = require("./Services/Configuration.Service");
const Root_Controller_1 = require("./Controllers/Root.Controller");
const Progression_Controller_1 = require("./Controllers/Progression.Controller");
const Progression_Service_1 = require("./Services/Progression.Service");
class AdvancementProgressServer {
    async Run() {
        // Services
        // TODO fix having to register stuff.
        ServicesManager_1.ServicesManager.registerService(new Configuration_Service_1.ConfigurationService());
        ServicesManager_1.ServicesManager.registerService(new Progression_Service_1.ProgressionService());
        // Get configuration
        let configManager = ServicesManager_1.ServicesManager.getService("ConfigurationService");
        let apiConfig = configManager.getConfiguration("api");
        // Create express app.
        const app = express();
        let port = apiConfig.port != null ? apiConfig.port : 35280;
        app.use(bodyParser.json());
        app.use(express.static('html'));
        // Set cors to allow all origins.
        let corsOptions = {
            origin: '*'
        };
        app.use(cors(corsOptions));
        let rootController = new Root_Controller_1.RootController();
        let profileController = new Profile_Controller_1.ProfileController();
        let progressionController = new Progression_Controller_1.ProgressionController();
        app.use("/api/", await rootController.getRouterInstance());
        app.use("/api/profile/", await profileController.getRouterInstance());
        app.use("/api/progression/", await progressionController.getRouterInstance());
        // start the Express server
        app.listen(port, server => {
            console.log(`Server started at http://localhost:${port}`);
        });
    }
}
exports.AdvancementProgressServer = AdvancementProgressServer;
if (require.main === module) {
    // Only run if this was the script that was run directly.
    new AdvancementProgressServer().Run().then(value => {
    }).catch(reason => {
        console.error(`Server failed \n${reason}`);
        throw reason;
    });
}
//# sourceMappingURL=App.js.map