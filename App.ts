import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import {ProfileController} from "./Controllers/Profile.Controller";
import {ServicesManager} from "./Services/ServicesManager";
import {ConfigurationService} from "./Services/Configuration.Service";
import {RootController} from "./Controllers/Root.Controller";
import {ProgressionController} from "./Controllers/Progression.Controller";
import {ProgressionService} from "./Services/Progression.Service";

export class AdvancementProgressServer {
    public async Run(): Promise<void> {
        // Services
        // TODO fix having to register stuff.
        ServicesManager.registerService(new ConfigurationService());
        ServicesManager.registerService(new ProgressionService());

        // Get configuration
        let configManager = ServicesManager.getService<ConfigurationService>("ConfigurationService");
        let apiConfig = configManager.getConfiguration<any>("api");
        // Create express app.
        const app = express();
        let port = apiConfig.port != null ? apiConfig.port : 35280;

        app.use(bodyParser.json());
        app.use(express.static('html'));

        // Set cors to allow all origins.
        let corsOptions = {
            origin: '*'
        };

        app.use(cors(corsOptions));

        let rootController = new RootController();
        let profileController = new ProfileController();
        let progressionController = new ProgressionController();
        app.use("/api/", await rootController.getRouterInstance());
        app.use("/api/profile/", await profileController.getRouterInstance());
        app.use("/api/progression/", await progressionController.getRouterInstance());

        // start the Express server
        app.listen(port, server => {
            console.log(`Server started at http://localhost:${port}`);
        });
    }
}

if (require.main === module) {
    // Only run if this was the script that was run directly.
    new AdvancementProgressServer().Run().then(value => {
    }).catch(reason => {
        console.error(`Server failed \n${reason}`);
        throw reason;
    });
}
