import {Injectable} from '@angular/core';
import {Statuses} from "../models/Statuses";
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProgressionService {

  constructor(private http: HttpClient) {
  }

  getData(): Observable<Statuses> {
    return this.http.get<Statuses>("/api/progression/")
  }
}
