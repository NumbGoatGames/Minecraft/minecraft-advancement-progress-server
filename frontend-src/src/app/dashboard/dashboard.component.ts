import { Component, OnInit } from '@angular/core';
import {ProgressionService} from "../progression.service";
import {Statuses} from "../../models/Statuses";
import {Observable} from "rxjs";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private progression: ProgressionService) { }

  ngOnInit() {
    this.progression.getData().subscribe(value => {
      this.processData(value);
    })
  }

  private processData(statuses: Statuses){
    this.data = statuses;
    this.totalNumberOfQuests = this.data.quests.length;
  }

  data: Statuses;
  totalNumberOfQuests: number;

}
