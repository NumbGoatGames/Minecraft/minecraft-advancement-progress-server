import { Component, OnInit } from '@angular/core';
import {ProgressionService} from "../progression.service";

@Component({
  selector: 'app-team-progression',
  templateUrl: './team-progression.component.html',
  styleUrls: ['./team-progression.component.css']
})
export class TeamProgressionComponent implements OnInit {

  constructor(private progressionService: ProgressionService) { }

  ngOnInit() {
  }

}
