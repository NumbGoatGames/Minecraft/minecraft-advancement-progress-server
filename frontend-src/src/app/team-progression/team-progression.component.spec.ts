import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamProgressionComponent } from './team-progression.component';

describe('TeamProgressionComponent', () => {
  let component: TeamProgressionComponent;
  let fixture: ComponentFixture<TeamProgressionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamProgressionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamProgressionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
