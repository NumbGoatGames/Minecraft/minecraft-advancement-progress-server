export interface Statuses {
  entry_id: string,
  entry: string,
  quests: Quest[]
}

export interface Quest {
  quest_id: string,
  quest: string,
  results: Result[]
}

export interface Result {
  team_id: string,
  team_name: string,
  complete: boolean
}
