export interface MinecraftConfiguration {
    servers: [MinecraftServerDefinition],

}

export interface MinecraftServerDefinition {
    name: string,
    path: string,
    advancementType?: string
    worldName?: string
}

export interface TriumphAdvancementServer extends MinecraftServerDefinition{
    advancementOption: string
}

let AdvancementTypes: string[] = [
    "UNKNOWN",
    "TRIUMPH",
    "FTBQUESTS"
];

export {AdvancementTypes};
