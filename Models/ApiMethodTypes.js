"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ApiMethodTypes;
(function (ApiMethodTypes) {
    ApiMethodTypes[ApiMethodTypes["GET"] = 0] = "GET";
    ApiMethodTypes[ApiMethodTypes["POST"] = 1] = "POST";
    ApiMethodTypes[ApiMethodTypes["PUT"] = 2] = "PUT";
    ApiMethodTypes[ApiMethodTypes["HEAD"] = 3] = "HEAD";
    ApiMethodTypes[ApiMethodTypes["DELETE"] = 4] = "DELETE";
    ApiMethodTypes[ApiMethodTypes["PATCH"] = 5] = "PATCH";
    ApiMethodTypes[ApiMethodTypes["OPTIONS"] = 6] = "OPTIONS";
})(ApiMethodTypes = exports.ApiMethodTypes || (exports.ApiMethodTypes = {}));
//# sourceMappingURL=ApiMethodTypes.js.map