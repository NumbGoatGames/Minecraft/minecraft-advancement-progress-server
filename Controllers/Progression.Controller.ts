import {BaseController} from "./BaseController";
import {Request, Response} from "express";
import {ApiMethodTypes} from "../Models/ApiMethodTypes";
import {ServicesManager} from "../Services/ServicesManager";
import {ConfigurationService} from "../Services/Configuration.Service";
import {MinecraftConfiguration} from "../Models/MinecraftConfiguration";
import {jsonData} from "@numb/mc-questing-progression";
import {ProgressionService} from "../Services/Progression.Service";

export class ProgressionController extends BaseController{
    private config: MinecraftConfiguration;
    private progressionService: ProgressionService;
    setup(): BaseController {
        // Retrieve config
        let configService = ServicesManager.getService<ConfigurationService>(ConfigurationService.name);
        this.config = configService.getConfiguration<MinecraftConfiguration>("minecraft");
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.")
        }

        this.checkConfiguration();

        this.progressionService = ServicesManager.getService<ProgressionService>(ProgressionService.name);

        let baseRoute = "";
        this.Routes = [
            {
                MethodType:ApiMethodTypes.GET,
                Path: baseRoute,
                Callback: async (req,res) => {
                    return await this.getProgressData(req,res);
                }
            }
        ];

        return super.setup();
    }

    private checkConfiguration(): void {
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.")
        }
        if (this.config.servers == null) {
            throw new Error("Minecraft requires installs to be defined.")
        }

        if(this.config.servers.length > 1){
            throw new Error("Multiple servers currently not supported :(");
        }
    }


    private async getProgressData(req,res): Promise<Response> {
        let data =  res.json(await this.progressionService.getProgressionData());
        return res.json(data);
    }

}

export interface AdvancementsData {
    totalNumberOfAdvancements: number
}
