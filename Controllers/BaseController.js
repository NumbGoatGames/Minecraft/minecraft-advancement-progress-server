"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const ApiMethodTypes_1 = require("../Models/ApiMethodTypes");
class BaseController {
    constructor() {
        // noinspection JSMismatchedCollectionQueryUpdate
        this.Routes = [];
        this.setup();
    }
    getRouterDeclaration() {
        if (this.ControllerRouter != null) {
            return this.ControllerRouter;
        }
        let router = express_1.Router();
        for (let route of this.Routes) {
            switch (route.MethodType) {
                case ApiMethodTypes_1.ApiMethodTypes.GET:
                    router.get(route.Path, route.Callback);
                    break;
                case ApiMethodTypes_1.ApiMethodTypes.POST:
                    router.post(route.Path, route.Callback);
                    break;
                case ApiMethodTypes_1.ApiMethodTypes.PUT:
                    router.put(route.Path, route.Callback);
                    break;
                case ApiMethodTypes_1.ApiMethodTypes.HEAD:
                    router.head(route.Path, route.Callback);
                    break;
                case ApiMethodTypes_1.ApiMethodTypes.DELETE:
                    router.delete(route.Path, route.Callback);
                    break;
                case ApiMethodTypes_1.ApiMethodTypes.PATCH:
                    router.patch(route.Path, route.Callback);
                    break;
                case ApiMethodTypes_1.ApiMethodTypes.OPTIONS:
                    router.options(route.Path, route.Callback);
                    break;
            }
        }
        this.ControllerRouter = router;
        return this.ControllerRouter;
    }
    setup() {
        return this;
    }
    async getRouterInstance() {
        return this.getRouterDeclaration();
    }
}
exports.BaseController = BaseController;
//# sourceMappingURL=BaseController.js.map