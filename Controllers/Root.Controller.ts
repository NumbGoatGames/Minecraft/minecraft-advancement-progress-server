import {BaseController} from "./BaseController";
import {ApiMethodTypes} from "../Models/ApiMethodTypes";
import {Response} from "express";
import {MinecraftConfiguration} from "../Models/MinecraftConfiguration";
import {ServicesManager} from "../Services/ServicesManager";
import {ConfigurationService} from "../Services/Configuration.Service";

export class RootController extends BaseController {
    constructor() {
        super();
    }

    private config: MinecraftConfiguration;

    public setup(): BaseController {
        let baseRoute = "";

        let configService = ServicesManager.getService<ConfigurationService>(ConfigurationService.name);
        this.config = configService.getConfiguration<MinecraftConfiguration>("minecraft");
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.")
        }
        this.checkConfiguration();

        this.Routes = [
            {
                MethodType: ApiMethodTypes.GET,
                Path: "",
                Callback: async (req, res) => {
                    return await this.statusCheckHndlr(req, res);
                }
            },
            {
                MethodType: ApiMethodTypes.GET,
                Path: "/server/",
                Callback: async (req,res) : Promise<Response> => {
                    return await this.getServers(req,res);
                }
            }

        ];

        return super.setup();
    }

    /**
     * Check configuration values to see if the required values are there.
     */
    private checkConfiguration(): void {
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.")
        }
        if (this.config.servers == null) {
            throw new Error("Minecraft requires installs to be defined.")
        }

        for(let server of this.config.servers){

        }
    }

    // noinspection JSMethodCanBeStatic
    /**
     * Provides a health check for the API.
     */
    private async statusCheckHndlr(req, res): Promise<Response> {
        return res.sendStatus(200);
    }

    private async getServers(req, res): Promise<Response> {
        // TODO check if server files exist.
        let names = this.config.servers.map(value => {
            return value.name
        });
        return res.json(names);
    }
}
