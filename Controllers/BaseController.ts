import {Router, Response, Request, Handler, NextFunction,} from 'express';
import {ApiMethodTypes} from "../Models/ApiMethodTypes";
import {create} from "domain";

export class BaseController {
    private ControllerRouter?: Router;
    // noinspection JSMismatchedCollectionQueryUpdate
    protected Routes: RouteDefinition[] = [];

    constructor(){
        this.setup();
    }

    protected getRouterDeclaration() : Router{
        if(this.ControllerRouter != null){
            return this.ControllerRouter;
        }
        let router: Router = Router();

        for (let route of this.Routes) {
            switch (route.MethodType) {
                case ApiMethodTypes.GET:
                    router.get(route.Path,route.Callback);
                    break;
                case ApiMethodTypes.POST:
                    router.post(route.Path,route.Callback);
                    break;
                case ApiMethodTypes.PUT:
                    router.put(route.Path,route.Callback);
                    break;
                case ApiMethodTypes.HEAD:
                    router.head(route.Path,route.Callback);
                    break;
                case ApiMethodTypes.DELETE:
                    router.delete(route.Path,route.Callback);
                    break;
                case ApiMethodTypes.PATCH:
                    router.patch(route.Path,route.Callback);
                    break;
                case ApiMethodTypes.OPTIONS:
                    router.options(route.Path,route.Callback);
                    break;

            }
        }
        this.ControllerRouter = router;
        return this.ControllerRouter;
    }

    public setup() : BaseController {
        return this;
    }

    public async getRouterInstance(): Promise<Router>{
        return this.getRouterDeclaration();
    }
}

export interface RouteDefinition {
    Path: string,
    MethodType: ApiMethodTypes,
    Callback(req: Request, res?: Response, next?: NextFunction): Promise<Response>
}
