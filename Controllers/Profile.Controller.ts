import {BaseController} from "./BaseController";
import {ApiMethodTypes} from "../Models/ApiMethodTypes";
import {Response,} from 'express';
import {ServicesManager} from "../Services/ServicesManager";
import {ConfigurationService} from "../Services/Configuration.Service";
import {MinecraftConfiguration} from "../Models/MinecraftConfiguration";
import * as fs from "fs";
import * as path from "path";
import * as rp from "request-promise-native";
import * as _ from "lodash";
import {RequestPromiseOptions} from "request-promise-native";
import {UriOptions} from "request";


export class ProfileController extends BaseController {
    private config: MinecraftConfiguration;

    constructor() {
        super();
    }

    public setup(): BaseController {
        // Retrieve config
        let configService = ServicesManager.getService<ConfigurationService>(ConfigurationService.name);
        this.config = configService.getConfiguration<MinecraftConfiguration>("minecraft");
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.")
        }

        this.checkConfiguration();
        // Define routes
        //  Api works with the base route of "/api/<install>/"
        let baseRoute = "/:install/";
        this.Routes = [
            {
                // Returns a list of all profiles.
                Path: "",
                MethodType: ApiMethodTypes.GET,
                Callback: async (req, res): Promise<Response> => {
                    return await this.getAllProfileIdsHndlr(req, res);
                }
            },
            {
                // Match profile UUID as a url parameter. Returns full Minecraft profile.
                Path: "/:uuid([a-z0-9]+-[a-z0-9]+-[a-z0-9]+-[a-z0-9]+-[a-z0-9]+)/",
                MethodType: ApiMethodTypes.GET,
                Callback: async (req, res): Promise<Response> => {
                    return await this.getFullMinecraftProfileHndrl(req, res);
                }
            },
            {
                // Returns a profile for :install
                Path: baseRoute + "",
                MethodType: ApiMethodTypes.GET,
                Callback: async (req, res, next) => {
                    return await this.getProfileIdsHndlr(req, res, next)
                }
            }
        ];
        return super.setup();
    }

    private checkConfiguration(): void {
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.")
        }
        if (this.config.servers == null) {
            throw new Error("Minecraft requires installs to be defined.")
        }
    }

    private async getProfileIdsFor(server: string): Promise<string[]> {
        let installDir = null;
        let worldName = null;

        for (let index in this.config.servers) {
            let def = this.config.servers[index];
            if (def.name == server) {
                installDir = def.path;
                worldName = def.worldName == null ? "world" : def.worldName;
                break;
            }
        }
        if (installDir == null) {
            return null;
        }
        let advancementFiles = fs.readdirSync(path.join(installDir, worldName, "advancements"));

        let uuids = [];
        for (let fileName of advancementFiles) {
            uuids.push(fileName.replace(".json", ""))
        }

        return uuids;
    }

    private async getAllProfileIdsHndlr(req, res): Promise<Response> {
        let allUuids = [];
        for (let installName of this.config.servers.map(value => value.name)) {
            let uuids = await this.getProfileIdsFor(installName);
            if (uuids == null) {
                continue;
            }
            uuids.forEach(value => allUuids.push(value));
        }

        return res.json(allUuids);
    }

    private async getProfileIdsHndlr(req, res, next): Promise<Response> {
        let install = req.params["install"];

        let uuids = await this.getProfileIdsFor(install);
        if (uuids == null) {
            res.message = `No install found for ${install}`;
            return res.sendStatus(400);
        }
        return res.json(uuids);
    }

    private async getFullMinecraftProfileHndrl(req, res): Promise<Response> {
        let uuid = req.params.uuid;
        if (uuid == null) {
            res.body = {"Error": "UUID must be valid."};
            return res.sendStatus(400);
        }

        let currentName = await this.getCurrentMinecraftName(uuid);
        let nameHistory = await this.getMinecraftNameHistory(uuid,currentName);

        let data = {
            name: currentName,
            nameHistory: nameHistory
        };

        return res.json(data);

    }

    private async getMinecraftNameHistory(uuid: string, exclude?: string) {
        // Remove hyphens
        uuid = uuid.split("-").join("");

        let result: {
            name: string,
            changeToAt?: number
        }[];
        let uri = `https://api.mojang.com/user/profiles/${uuid}/names`;
        try {
            result = JSON.parse(await rp.get(uri));
        } catch (e) {
            throw e;
        }

        _.remove(result,function(n) {
            return n.name == exclude;
        });

        return result;
    }

    private async getCurrentMinecraftName(uuid: string) {
        let nameHistory: {
            "name": string,
            "changedToAt"?: number
        }[];
        try {
            nameHistory = await this.getMinecraftNameHistory(uuid);
        } catch (e) {
            throw e;
        }

        if (nameHistory.length == 0) {
            throw new Error(`No profile found for ${uuid}`)
        }

        let latestName = null;
        let latestTime: number | null = null;

        for (let nameAtTime of nameHistory) {
            if (latestTime == null) {
                latestName = nameAtTime.name;
                if (nameAtTime.changedToAt != null) {
                    latestTime = nameAtTime.changedToAt;
                } else {
                    latestTime = Number.MIN_SAFE_INTEGER
                }
                continue;
            }

            if (nameAtTime.changedToAt != null && nameAtTime.changedToAt > latestTime) {
                latestTime = nameAtTime.changedToAt;
                latestName = nameAtTime.name;
            }
        }

        if(latestName == null) {
            throw new Error(`Failed to find the current name for ${uuid}`);
        }

        return latestName;
    }

    /**
     * Handler for not implemented routes.
     */
    private async notImplementedHndlr(req, res): Promise<Response> {
        res.statusMessage = "Not implemented.";
        return res.sendStatus("500");
    }
}
