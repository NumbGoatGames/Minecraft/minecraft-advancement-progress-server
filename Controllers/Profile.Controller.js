"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseController_1 = require("./BaseController");
const ApiMethodTypes_1 = require("../Models/ApiMethodTypes");
const ServicesManager_1 = require("../Services/ServicesManager");
const Configuration_Service_1 = require("../Services/Configuration.Service");
const fs = require("fs");
const path = require("path");
const rp = require("request-promise-native");
const _ = require("lodash");
class ProfileController extends BaseController_1.BaseController {
    constructor() {
        super();
    }
    setup() {
        // Retrieve config
        let configService = ServicesManager_1.ServicesManager.getService(Configuration_Service_1.ConfigurationService.name);
        this.config = configService.getConfiguration("minecraft");
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.");
        }
        this.checkConfiguration();
        // Define routes
        //  Api works with the base route of "/api/<install>/"
        let baseRoute = "/:install/";
        this.Routes = [
            {
                // Returns a list of all profiles.
                Path: "",
                MethodType: ApiMethodTypes_1.ApiMethodTypes.GET,
                Callback: async (req, res) => {
                    return await this.getAllProfileIdsHndlr(req, res);
                }
            },
            {
                // Match profile UUID as a url parameter. Returns full Minecraft profile.
                Path: "/:uuid([a-z0-9]+-[a-z0-9]+-[a-z0-9]+-[a-z0-9]+-[a-z0-9]+)/",
                MethodType: ApiMethodTypes_1.ApiMethodTypes.GET,
                Callback: async (req, res) => {
                    return await this.getFullMinecraftProfileHndrl(req, res);
                }
            },
            {
                // Returns a profile for :install
                Path: baseRoute + "",
                MethodType: ApiMethodTypes_1.ApiMethodTypes.GET,
                Callback: async (req, res, next) => {
                    return await this.getProfileIdsHndlr(req, res, next);
                }
            }
        ];
        return super.setup();
    }
    checkConfiguration() {
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.");
        }
        if (this.config.servers == null) {
            throw new Error("Minecraft requires installs to be defined.");
        }
    }
    async getProfileIdsFor(server) {
        let installDir = null;
        let worldName = null;
        for (let index in this.config.servers) {
            let def = this.config.servers[index];
            if (def.name == server) {
                installDir = def.path;
                worldName = def.worldName == null ? "world" : def.worldName;
                break;
            }
        }
        if (installDir == null) {
            return null;
        }
        let advancementFiles = fs.readdirSync(path.join(installDir, worldName, "advancements"));
        let uuids = [];
        for (let fileName of advancementFiles) {
            uuids.push(fileName.replace(".json", ""));
        }
        return uuids;
    }
    async getAllProfileIdsHndlr(req, res) {
        let allUuids = [];
        for (let installName of this.config.servers.map(value => value.name)) {
            let uuids = await this.getProfileIdsFor(installName);
            if (uuids == null) {
                continue;
            }
            uuids.forEach(value => allUuids.push(value));
        }
        return res.json(allUuids);
    }
    async getProfileIdsHndlr(req, res, next) {
        let install = req.params["install"];
        let uuids = await this.getProfileIdsFor(install);
        if (uuids == null) {
            res.message = `No install found for ${install}`;
            return res.sendStatus(400);
        }
        return res.json(uuids);
    }
    async getFullMinecraftProfileHndrl(req, res) {
        let uuid = req.params.uuid;
        if (uuid == null) {
            res.body = { "Error": "UUID must be valid." };
            return res.sendStatus(400);
        }
        let currentName = await this.getCurrentMinecraftName(uuid);
        let nameHistory = await this.getMinecraftNameHistory(uuid, currentName);
        let data = {
            name: currentName,
            nameHistory: nameHistory
        };
        return res.json(data);
    }
    async getMinecraftNameHistory(uuid, exclude) {
        // Remove hyphens
        uuid = uuid.split("-").join("");
        let result;
        let uri = `https://api.mojang.com/user/profiles/${uuid}/names`;
        try {
            result = JSON.parse(await rp.get(uri));
        }
        catch (e) {
            throw e;
        }
        _.remove(result, function (n) {
            return n.name == exclude;
        });
        return result;
    }
    async getCurrentMinecraftName(uuid) {
        let nameHistory;
        try {
            nameHistory = await this.getMinecraftNameHistory(uuid);
        }
        catch (e) {
            throw e;
        }
        if (nameHistory.length == 0) {
            throw new Error(`No profile found for ${uuid}`);
        }
        let latestName = null;
        let latestTime = null;
        for (let nameAtTime of nameHistory) {
            if (latestTime == null) {
                latestName = nameAtTime.name;
                if (nameAtTime.changedToAt != null) {
                    latestTime = nameAtTime.changedToAt;
                }
                else {
                    latestTime = Number.MIN_SAFE_INTEGER;
                }
                continue;
            }
            if (nameAtTime.changedToAt != null && nameAtTime.changedToAt > latestTime) {
                latestTime = nameAtTime.changedToAt;
                latestName = nameAtTime.name;
            }
        }
        if (latestName == null) {
            throw new Error(`Failed to find the current name for ${uuid}`);
        }
        return latestName;
    }
    /**
     * Handler for not implemented routes.
     */
    async notImplementedHndlr(req, res) {
        res.statusMessage = "Not implemented.";
        return res.sendStatus("500");
    }
}
exports.ProfileController = ProfileController;
//# sourceMappingURL=Profile.Controller.js.map