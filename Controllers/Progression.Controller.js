"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseController_1 = require("./BaseController");
const ApiMethodTypes_1 = require("../Models/ApiMethodTypes");
const ServicesManager_1 = require("../Services/ServicesManager");
const Configuration_Service_1 = require("../Services/Configuration.Service");
const Progression_Service_1 = require("../Services/Progression.Service");
class ProgressionController extends BaseController_1.BaseController {
    setup() {
        // Retrieve config
        let configService = ServicesManager_1.ServicesManager.getService(Configuration_Service_1.ConfigurationService.name);
        this.config = configService.getConfiguration("minecraft");
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.");
        }
        this.checkConfiguration();
        this.progressionService = ServicesManager_1.ServicesManager.getService(Progression_Service_1.ProgressionService.name);
        let baseRoute = "";
        this.Routes = [
            {
                MethodType: ApiMethodTypes_1.ApiMethodTypes.GET,
                Path: baseRoute,
                Callback: async (req, res) => {
                    return await this.getProgressData(req, res);
                }
            }
        ];
        return super.setup();
    }
    checkConfiguration() {
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.");
        }
        if (this.config.servers == null) {
            throw new Error("Minecraft requires installs to be defined.");
        }
        if (this.config.servers.length > 1) {
            throw new Error("Multiple servers currently not supported :(");
        }
    }
    async getProgressData(req, res) {
        let data = res.json(await this.progressionService.getProgressionData());
        return res.json(data);
    }
}
exports.ProgressionController = ProgressionController;
//# sourceMappingURL=Progression.Controller.js.map