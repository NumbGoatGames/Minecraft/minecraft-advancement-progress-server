"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseController_1 = require("./BaseController");
const ApiMethodTypes_1 = require("../Models/ApiMethodTypes");
const ServicesManager_1 = require("../Services/ServicesManager");
const Configuration_Service_1 = require("../Services/Configuration.Service");
class RootController extends BaseController_1.BaseController {
    constructor() {
        super();
    }
    setup() {
        let baseRoute = "";
        let configService = ServicesManager_1.ServicesManager.getService(Configuration_Service_1.ConfigurationService.name);
        this.config = configService.getConfiguration("minecraft");
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.");
        }
        this.checkConfiguration();
        this.Routes = [
            {
                MethodType: ApiMethodTypes_1.ApiMethodTypes.GET,
                Path: "",
                Callback: async (req, res) => {
                    return await this.statusCheckHndlr(req, res);
                }
            },
            {
                MethodType: ApiMethodTypes_1.ApiMethodTypes.GET,
                Path: "/server/",
                Callback: async (req, res) => {
                    return await this.getServers(req, res);
                }
            }
        ];
        return super.setup();
    }
    /**
     * Check configuration values to see if the required values are there.
     */
    checkConfiguration() {
        if (this.config == null) {
            throw new Error("Requires \"minecraft\" to be defined in configuration.");
        }
        if (this.config.servers == null) {
            throw new Error("Minecraft requires installs to be defined.");
        }
        for (let server of this.config.servers) {
        }
    }
    // noinspection JSMethodCanBeStatic
    /**
     * Provides a health check for the API.
     */
    async statusCheckHndlr(req, res) {
        return res.sendStatus(200);
    }
    async getServers(req, res) {
        // TODO check if server files exist.
        let names = this.config.servers.map(value => {
            return value.name;
        });
        return res.json(names);
    }
}
exports.RootController = RootController;
//# sourceMappingURL=Root.Controller.js.map